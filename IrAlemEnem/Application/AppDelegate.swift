//
//  AppDelegate.swift
//  IrAlemEnem
//
//  Created by Gilson Gil on 7/1/16.
//  Copyright © 2016 FTD. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    return true
  }
}
